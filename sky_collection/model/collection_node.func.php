<?php
/**
 *
 * @author tiankong <tianakong@aliyun.com>
 * @version 1.0
 */

// ------------> 最原生的 CURD，无关联其他数据。

function collection_node__create($arr) {
    $r = db_create('collection_node', $arr);
    return $r;
}

function collection_node__update($id, $arr) {
    $r = db_update('collection_node', array('id'=>$id), $arr);
    return $r;
}

function collection_node__read($id) {
    $data = db_find_one('collection_node', array('id'=>$id));
    return $data;
}

function collection_node__delete($id) {
    $r = db_delete('collection_node', array('id'=>$id));
    return $r;
}

function collection_node__find($cond = array(), $orderby = array(), $page = 1, $pagesize = 20) {
    $lists = db_find('collection_node', $cond, $orderby, $page, $pagesize);
    return $lists;
}


function collection_node_count($cond = array()) {
    $n = db_count('collection_node', $cond);
    return $n;
}

function collection_node_read_cond($cond) {
    $data = db_find_one('collection_node', $cond);
    return $data;
}

?>