<?php
/**
 *
 * @author tiankong <tianakong@aliyun.com>
 * @version 1.0
 */

// ------------> 最原生的 CURD，无关联其他数据。

function collection_crontab_log__create($arr) {
    $r = db_create('collection_crontab_log', $arr);
    return $r;
}

function collection_crontab_log__update($id, $arr) {
    $r = db_update('collection_crontab_log', array('id'=>$id), $arr);
    return $r;
}

function collection_crontab_log__read($id) {
    $data = db_find_one('collection_crontab_log', array('id'=>$id));
    return $data;
}

function collection_crontab_log__delete($id) {
    $r = db_delete('collection_crontab_log', array('id'=>$id));
    return $r;
}

function collection_crontab_log__find($cond = array(), $orderby = array(), $page = 1, $pagesize = 20) {
    $lists = db_find('collection_crontab_log', $cond, $orderby, $page, $pagesize);
    return $lists;
}

function collection_crontab_log__read_day($date) {
    $data = db_find_one('collection_crontab_log', array('day_date'=>$date));
    return $data;
}

function collection_crontab_log__read_cond($cond) {
    $data = db_find_one('collection_crontab_log', $cond);
    return $data;
}

?>