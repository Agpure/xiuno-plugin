<?php
!defined('DEBUG') AND exit('Access Denied.');
/**
 *
 * @author tiankong <tianakong@aliyun.com>
 * @version 1.0
 */
error_reporting(0);
$action = param(1);
if ($action == 'post') {
    //计划任务发帖
    $nodes = collection_node__find(array('time_post_is' => 1), array(), 1, 1000);
    if ($nodes) {
        include APP_PATH . 'plugin/sky_collection/lib/func.php';
        foreach ($nodes as $node) {
            if(!$node['time_post_times']) {
                return false;
            }
            $time_post_times_conf = explode(chr(10), $node['time_post_times']);
            if(!$time_post_times_conf) {
                return false;
            }
            foreach ($time_post_times_conf as $val) {
                $time_number = explode('|', $val);
                $node_post_time = strtotime($time_number[0]);
                $res = collection_crontab_log__read_cond(array('day_date'=>$node_post_time, 'node_id'=>$node['id']));
                if(!$res) {
                    if ($node_post_time <= time()) {
                        $time_post_number = isset($time_number[1]) ? (int)$time_number[1] : 999999;
                        $contents = collection_content__find(array('nodeid' => $node['id'], 'status' => 1), array('id' => -1), 1, $time_post_number);
                        $content_ids = array();
                        if ($contents) {
                            foreach ($contents as $v) {
                                $content_ids[] = $v['id'];
                            }
                        }
                        $forum_ids = json_decode($node['time_post_forumids'], true);
                        if ($content_ids && $forum_ids) {
                            sky_import_data($node['id'], $forum_ids, $content_ids);
                            $arr = array();
                            $arr['day_date'] = $node_post_time;
                            $arr['is_post'] = 1;
                            $arr['node_id'] = $node['id'];
                            $arr['post_number'] = count($contents);
                            collection_crontab_log__create($arr);
                            echo '任务完成';
                        }
                    }
                }
            }
        }
    }
} else if ($action == 'urls') {
    //计划任务采集网址
} else if ($action == 'contents') {
    //计划任务采集内容
}


?>