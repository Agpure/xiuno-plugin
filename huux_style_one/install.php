<?php

/*
	Xiuno BBS 4.0 大白_笔记模板
*/

!defined('DEBUG') AND exit('Forbidden');




$setting = setting_get('huux_style_one');

if(empty($setting)) {
	$setting = array('hso_logo'=>'大白笔记', 'hso_forum_info'=>1, 'hso_background'=>'', 'hso_background_color'=>'#ededed', 'hso_background_repeat'=>'repeat', 'hso_background_position'=>'50% 0%','hso_background_attachment'=>'scroll', 'hso_site_brief'=>1, 'hso_site_count'=>1, 'hso_site_user'=>1, 'hso_thread_userthread'=>1, 'hso_thread_menu'=>1,'hso_index_menu'=>1,'hso_title_length'=>0, 'hso_thread_back'=>2, 'hso_threadlist1_avatar'=>1, 'hso_threadlist1_imgs'=>0, 'hso_thread_user'=>1,'hso_thread_cardfooter'=>1, 'hso_site_headernav'=>0,'hso_col_index'=>1,'hso_forum1_pagination_header'=>1);
	setting_set('huux_style_one', $setting);
}



// 初始化KV方案  ## 请勿删除 ##
$kv = kv_get('dabai_plugin');
if(!$kv) {
	$kv = array('huux_style_one' =>'{"id":"huux_style_one","name":"大白笔记模板","type":"模板","set":1}' );
	kv_set('dabai_plugin', $kv);
}else{
	if (!in_array("huux_style_one", $kv)){
		$kv = $kv+array('huux_style_one' =>'{"id":"huux_style_one","name":"大白笔记模板","type":"模板","set":1}' );
	}
	kv_set('dabai_plugin', $kv);
}





?>