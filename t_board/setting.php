<?php

!defined('DEBUG') AND exit('Access Denied.');

if ($method == 'GET') {
    $kv = kv_get('t_board');
	$input['content'] = form_text('content',$kv['content']);
    include _include(APP_PATH.'plugin/t_board/setting.htm');
} else {
    $kv = array();
    $kv['content'] = param('content');
	$kv['style'] = param('style');
    kv_set('t_board', $kv);
    message(0,lang('save_successfully'));
}

?>