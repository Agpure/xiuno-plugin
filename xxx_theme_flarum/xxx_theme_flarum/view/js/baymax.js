$(function() {
  //后退
  $(".win-back").click(function() {
      window.history.back(-1);
  });
  //刷新
  $(".win-refresh").click(function() {
      window.location.reload();
  });
  //返回顶部
  $(window).scroll(function(){
    if ($(window).scrollTop()>100){
      $(".back-top").fadeIn(1000);
    }
    else
    {
      $(".back-top").fadeOut(1000);
    }
  });
  $(".win-backtop").click(function() {
      $('body,html').animate({
          scrollTop: 0
      }, 1000);
      return false;
  });
  //回到底部
  $('.win-backbottom').click(function(){
    $('html,body').animate({
        scrollTop:$('#quick_reply_form').offset().top
    }, 500);
  });
  // 切换显示隐藏
  $(".js-hide").click(function() {
      $("#js-block").slideUp();
  });
  $(".js-show").click(function() {
      $("#js-block").slideDown();
  });
  // 鼠标经过显示帖子列表
  $(".Navigation-back, .DiscussionPage-list").hover(function() {
      $(".App").addClass("paneShowing");
      },function(){
          $(".App").removeClass("paneShowing");
  });
  //静止在顶部或尾部
  $(".affix").each(function() {
      var e = $(this);
      var style = e.attr("data-style");
      var top = e.attr("data-offset-fixed");
      if (top == null) {
          top = e.offset().top;
      } else {
          top = e.offset().top - parseInt(top);
      };
      if (style == null) {
          style = "scrolled";
      };
      $(window).bind("scroll", function() {
          var thistop = top - $(window).scrollTop();
          if (style == "scrolled" && thistop < 0) {
              e.addClass("scrolled");
          } else {
              e.removeClass("scrolled");
          };
          var thisbottom = top - $(window).scrollTop() - $(window).height();
          if (style == "fixed-bottom" && thisbottom > 0) {
              e.addClass("fixed-bottom");
          } else {
              e.removeClass("fixed-bottom");
          };
      });
  });
  // 拖动改变层大小
  $(function(){
      var src_posi_Y = 0, dest_posi_Y = 0, move_Y = 0, is_mouse_down = false, destHeight = 460;
      $(".Composer-handle")
      .mousedown(function(e){
          src_posi_Y = e.pageY;//鼠标指针的位置
          is_mouse_down = true;
      });
      $(document).bind("click mouseup",function(e){
          if(is_mouse_down){
            is_mouse_down = false;
          }
      })
      .mousemove(function(e){
          dest_posi_Y = e.pageY;
          move_Y = dest_posi_Y - src_posi_Y;
          src_posi_Y = dest_posi_Y;
          destHeight = $(".Composer").height() - move_Y;
          if(is_mouse_down){
              $(".Composer").css("height", destHeight > 460 ? destHeight : 460);
          }
      });
  });
  // 点赞动画
  $(document).ready(function()
  {
  $('body').on("click",'.heart',function()
    {
      var A=$(this).attr("id");
      var B=A.split("like");
        var messageID=B[1];
        var C=parseInt($("#likeCount"+messageID).html());
      $(this).css("background-position","")
        var D=$(this).attr("rel");
        if(D === 'like')
        {
          $("#likeCount"+messageID).html(C+1);
          $(this).addClass("heartAnimation").attr("rel","unlike");
        }
        else
        {
          $("#likeCount"+messageID).html(C-1);
          $(this).removeClass("heartAnimation").attr("rel","like");
          $(this).css("background-position","left");
        }
    });
  });
  //图片自适应
  // var w = $(".thread_post").width();
  // $(".thread_post img").each(function(){
  //     var img_w = $(this).width();
  //     var img_h = $(this).height();
  //     if(img_w>w){
  //         var height = (w*img_h)/img_w;
  //         $(this).css({"width":w,"height":height});
  //     }
  // });
});
//Off Canvas 导航
$(function(){function a(){e.toggleClass(j),d.toggleClass(i),f.toggleClass(k),g.toggleClass(l)}function b(){e.addClass(j),d.animate({left:"0px"},n),f.animate({left:o},n),g.animate({left:o},n)}function c(){e.removeClass(j),d.animate({left:"-"+o},n),f.animate({left:"0px"},n),g.animate({left:"0px"},n)}var d=$(".pushy"),e=$("body"),f=$("#offcanvas"),g=$(".push"),h=$(".site-overlay"),i="pushy-left pushy-open",j="pushy-active",k="offcanvas-push",l="push-push",m=$(".Navigation-drawer, .pushy a"),n=200,o=d.width()+"px";if(cssTransforms3d=function(){var a=document.createElement("p"),b=!1,c={webkitTransform:"-webkit-transform",OTransform:"-o-transform",msTransform:"-ms-transform",MozTransform:"-moz-transform",transform:"transform"};document.body.insertBefore(a,null);for(var d in c)void 0!==a.style[d]&&(a.style[d]="translate3d(1px,1px,1px)",b=window.getComputedStyle(a).getPropertyValue(c[d]));return document.body.removeChild(a),void 0!==b&&b.length>0&&"none"!==b}())m.click(function(){a()}),h.click(function(){a()});else{d.css({left:"-"+o}),f.css({"overflow-x":"hidden"});var p=!0;m.click(function(){p?(b(),p=!1):(c(),p=!0)}),h.click(function(){p?(b(),p=!1):(c(),p=!0)})}});